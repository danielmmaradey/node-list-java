package node;

public class ListNode {
	
	public void display(Node head) {
		if (head == null) {
			return;
		}
		
		Node current = head;
		
		// loop each element till end of the list
		// last node points to null
		
		while(current != null) {
			
			//print current element's data
			
			System.out.print(current.data + "-->");
			
			//move to next element
			
			current = current.next;
		}
		
		//System.out.print(current);
	}
	
	public Node reverse(Node head) {
		if (head == null) {
			return head;
		}
		
		Node current = head;
		Node previous = null;
		Node next = null;
		
		while (current != null) {
			next = current.next;
			current.next = previous;
			previous = current;
			current = next;
		}
		
		return previous;
	}
	
	public int length(Node head) {
		if (head == null) {
			return 0;
		}
		
		int count = 0;
		Node current = head;
		
		while(current != null) {
			count++;
			current = current.next;
		}
		
		return count;
				
	}
	
}
