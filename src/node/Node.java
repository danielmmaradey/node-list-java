package node;

public class Node {
	
	public Node next;
	public int data;
	
	public Node() {
		this.next = null;
		this.data = 0;
	}
	
	public Node(int data) {
		this.next = null;
		this.data = data;
	}
}
