package node;

public class Main {
	
	public static void main(String[] args) {
		
		/*Let's create a linked list 1-->2-->3*/
		
		// 1 will be the head of linked list;
		
		Node head = new Node(1);
		Node n2 = new Node(2);
		Node n3 = new Node(3);
		
		// Attach them together to form linked list;
		
		head.next = n2;
		n2.next = n3;
		
		// function to display linked list
		
		ListNode list = new ListNode();
		list.display(head);
		
		// function to display reverse linked list
		
		Node reverse = list.reverse(head);
		list.display(reverse);
		
		// function to display the length of linked list
		
		System.out.println("\n\nLength of linked list is : " + list.length(reverse));
	}
	
}
